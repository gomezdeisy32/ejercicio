/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicionotas;

import java.util.Scanner;

/**
 *
 * @author Tatta
 */



public class Alumno {
    
Scanner leer = new Scanner(System.in);
    
    private String nombre;
    private String apellido;
    private double identificacion;
    private double edad;
    private double nota;
    
Alumno alumno[] = new Alumno[2];

public Alumno()
{
nombre = "";
apellido = "";
identificacion = 0;
edad = 0;
nota = 0;
}

public Alumno(String n, String m, double jj,double j, double not)
{
       
 nombre = n;
apellido = m;
identificacion = j;
edad = jj;
nota = not;
    
}

public void cargardatos() {
    for(int i=0; i<alumno.length;i++)
        {
            System.out.println("Ingresa el nombre del estudiante");
            nombre=leer.nextLine();
            System.out.println("Ingresa el apellido del estudiante");
            apellido=leer.nextLine();
            System.out.println("Ingresa la identificacion del estudiante");
            identificacion=leer.nextDouble();
            System.out.println("Ingresa la edad del estudiante");
            edad=leer.nextDouble();
           System.out.println("Ingresa la  del estudiante");
            setNota(leer.nextDouble());
}
}

public void imprimirArray() {
         System.out.println("Nombre"+"    Apellido"+"      Identificacion"+"   Edad"+"  Prom Mat"+"   Prom Fis"+ "   Prom Progra"+ "   Prom Quim");
            System.out.println();
            for (int a = 0; a < alumno.length; a++){
                System.out.println(alumno[a].getnombre()+"     "+alumno[a].getapellido()+"        "+alumno[a].getidentificacion()+"        "+alumno[a].getedad()+" ");
            }
           System.out.println();
   }


public void setnombre(String n)
{
    nombre = n;
}

public String getnombre()
{
    return nombre;
}

public void setapellido(String n)
{
    apellido = n;
}

public String getapellido()
{
    return apellido;
}

public void setidentificacion(double n)
{
    this.identificacion=n;
}

public double getidentificacion()
{
    return identificacion;
}

public void setedad(double ed)
{
    this.edad=ed;
}

public double getedad()
{
    return edad;
}

    /**
     * @return the nota
     */
    public double getNota() {
        return nota;
    }

    /**
     * @param nota the nota to set
     */
    public void setNota(double nota) {
        this.nota = nota;
    }


}